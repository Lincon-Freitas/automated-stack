# Provider config for bootstrap
provider "rancher2" {
  alias     = "bootstrap"

  api_url   = format("https://rancher.%s", var.DOMAIN)
  bootstrap = true
  insecure  = true
}

# Create a new rancher2_bootstrap using bootstrap provider config
resource "rancher2_bootstrap" "admin" {
  provider  = rancher2.bootstrap

  password  = var.RANCHER_PASS
  telemetry = true
  depends_on = [
    helm_release.rancher_server,
  ]
}

# Provider config for admin
provider "rancher2" {
  alias     = "admin"

  api_url   = format("https://rancher.%s", var.DOMAIN)
  token_key = rancher2_bootstrap.admin.token
  insecure  = true
}

# Cloud credentials for rancher
resource "rancher2_cloud_credential" "default_cloud_credential" {
  provider     = rancher2.admin
  
  name         = "default-cloud-credential"
  description  = "Default cloud credential"
  amazonec2_credential_config {
    access_key = var.AWS_ACCESS_AWS
    secret_key = var.AWS_SECRET_AWS
  }
}