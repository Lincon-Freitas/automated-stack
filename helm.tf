provider "helm" {
  kubernetes {
    config_path = "./kube_config_cluster.yml"
  }
}

# Install cert-manager helm chart
resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  
  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  namespace        = "cert-manager"
  version          = "v1.1.0"
  create_namespace = true
  depends_on = [
    rke_cluster.cluster,
    local_file.kube_cluster_yaml,
  ]
}

# Install Rancher helm chart
resource "helm_release" "rancher_server" {
  name             = "rancher"

  repository       = "https://releases.rancher.com/server-charts/stable"
  chart            = "rancher"
  version          = "v2.5.5"
  namespace        = "cattle-system"
  create_namespace = true

  depends_on = [
    helm_release.cert_manager,
  ]

  set {
    name  = "hostname"
    value = format("rancher.%s", var.DOMAIN)
  }
  set {
    name  = "certmanager.version"
    value = "v1.1.0"
  }
  set {
    name  = "tls"
    value = "external"
  }
  set {
    name  = "annotations.\"meta\\.helm\\.sh/release-name\""
    value = "rancher-operator-crd"
  }
  set {
    name  = "annotations.\"meta\\.helm\\.sh/release-namespace\""
    value = "rancher-operator-system"
  }
  set {
    name  = "labels.app.\"kubernetes\\.io/managed-by\""
    value = "Helm"
  }
}