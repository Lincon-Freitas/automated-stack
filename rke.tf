# Passing variables to the module
module "nodes" {
  source             = "./aws_rke"
  region             = var.AWS_REGION
  access_key         = var.AWS_ACCESS_AWS
  secret_key         = var.AWS_SECRET_AWS
  instance_type      = var.INSTANCE_TYPE
  cluster_id         = var.CLUSTER_ID
  docker_install_url = var.DOCKER_URL
  domain             = var.DOMAIN
  node_count         = var.NODE_COUNT
  zone_id            = var.ZONE_ID
}

# Create the RKE cluster
resource "rke_cluster" "cluster" {
  
  # Defining the cloud provider
  cloud_provider {
    name = "aws"
  }
  
  # Define CP and ETCD node
  nodes {
    address = module.nodes.addresses[0]
    internal_address = module.nodes.internal_ips[0]
    user    = module.nodes.ssh_username
    ssh_key = module.nodes.private_key
    role    = ["controlplane", "etcd", "worker"]
  }

  # Define one worker
  nodes {
    address = module.nodes.addresses[1]
    internal_address = module.nodes.internal_ips[1]
    user    = module.nodes.ssh_username
    ssh_key = module.nodes.private_key
    role    = ["controlplane", "etcd", "worker"]
  }

  ingress {
    provider = "nginx"
    options = {
      use-forwarded-headers = true
    }
  }

}

# Export the cluster config file
resource "local_file" "kube_cluster_yaml" {
  filename = "./kube_config_cluster.yml"
  content  = rke_cluster.cluster.kube_config_yaml
}