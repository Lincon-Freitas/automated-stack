variable "AWS_ACCESS_AWS" {
  default = ""
}
variable "AWS_SECRET_AWS" {
  default = ""
}
variable "AWS_REGION" {
  default = "us-east-1"
}
variable "CLUSTER_ID" {
  default = "rke"
}
variable "DOCKER_URL" {
  default = "https://releases.rancher.com/install-docker/19.03.sh"
}
variable "DOMAIN" {
  default = ""
}
variable "INSTANCE_TYPE" {
  default = "t3a.medium"
}
variable "NODE_COUNT" {
  default = "2"
}
variable "RANCHER_PASS" {
  default = ""
}
variable "ZONE_ID" {
  default = ""
}