terraform {
  required_providers {
    rke = {
      source  = "rancher/rke"
      version = "1.1.7"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.25.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0.2"
    }
    rancher2 = {
      source = "rancher/rancher2"
      version = "1.11.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.0.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 3.0.0"
    }
  }
  required_version = ">= 0.13"
}