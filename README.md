# Rancher on AWS with Terraform

This project creates an rke cluster on top of ec2 instances which we will install rancher. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- terraform
- bash
- kubectl
- AWS account
- git

### Installing

Clone the this repository to your local machine:

```
git clone https://gitlab.com/Lincon-Freitas/automated-stack.git
```

Rename the **terraform.tfvars.example** to **terraform.tfvars**

```
mv terraform.tfvars.example terraform.tfvars
```

Change the following values (the others can be changed, but they are optional):

```
AWS_ACCESS_AWS       = "xxx"
AWS_SECRET_AWS       = "xxx"
#AWS_REGION          = ""
#CLUSTER_ID          = ""
#DOCKER_URL          = ""
#INSTANCE_TYPE       = ""
#NODE_COUNT          = ""
DOMAIN               = "yourdomain.com"
RANCHER_PASS         = "test@123"
ZONE_ID              = "xxx"
```

Lets download the plugins:

```
terraform init
```

Check if everything is alright:

```
terraform plan
```

Now it is time to run our code:

```
terraform apply --auto-approve
```

### Accessing the rancher console

The url to access rancher is: **https://rancher.yourdomain.com**

### Destroying environment

To remove all the resources:

```
terraform destroy --auto-approve
```

## Built With

* [Terraform](https://www.terraform.io/)
* [Rancher](https://rancher.com/)
* [AWS](https://aws.amazon.com/)


## Authors

* **Lincon Freitas** - *Initial work* - [Git](https://gitlab.com/Lincon-Freitas)
                                        [LinkedIn](https://www.linkedin.com/in/lincon-freitas)

* Based in the plugin: https://github.com/rancher/terraform-provider-rke