# Define tags
locals {
  cluster_id_tag = {
    "kubernetes.io/cluster/${var.cluster_id}" = "owned"
    Name                                      = "rke-cluster"
  }
}

# Check number of AZs
data "aws_availability_zones" "az" {
}

# Define a subnet
resource "aws_default_subnet" "default" {
  count             = length(data.aws_availability_zones.az.names)

  availability_zone = data.aws_availability_zones.az.names[count.index]
  tags              = local.cluster_id_tag
}

# Create a security group
resource "aws_security_group" "allow-all" {
  name        = "rke-default-security-group"
  description = "RKE Cluster SG"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.cluster_id_tag
}

# Create EC2 instances for RKE
resource "aws_instance" "rke-node" {
  count                  = var.node_count
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.rke-node-key.id
  iam_instance_profile   = aws_iam_instance_profile.rke-aws.name
  vpc_security_group_ids = [aws_security_group.allow-all.id]
  tags                   = local.cluster_id_tag
  
  root_block_device {
    volume_size = 20
  }

  # Install docker and add ubuntu in its group
  provisioner "remote-exec" {
    connection {
      host        = coalesce(self.public_ip, self.private_ip)
      type        = "ssh"
      user        = "ubuntu"
      private_key = tls_private_key.node-key.private_key_pem
    }

    inline = [
      "curl ${var.docker_install_url} | sh",
      "sudo usermod -a -G docker ubuntu",
    ]
  }
}