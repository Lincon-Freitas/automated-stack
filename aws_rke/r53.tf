# Create a Route53 record for Rancher
resource "aws_route53_record" "rancher_dns" {
  zone_id = var.zone_id
  name    = format("rancher.%s", var.domain)
  type    = "A"

  alias {
    name                   = aws_lb.rke_elb.dns_name
    zone_id                = aws_lb.rke_elb.zone_id
    evaluate_target_health = false
  }
}

# Create a Route53 record to validate the tls certificate
resource "aws_route53_record" "cert_record" {

  allow_overwrite = true
  name            = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_name
  records         = [tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_value]
  type            = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_type
  ttl             = 60
  zone_id         = var.zone_id
}