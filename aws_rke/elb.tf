# Create an ELB
resource "aws_lb" "rke_elb" {
  name               = "rke-elb" 
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.elb_sg.id]
  subnets            = tolist(aws_default_subnet.default[*].id)
  tags = {
    Name = "rke-elb" 
  }
}

# Create a target group
resource "aws_lb_target_group" "rke_tg" {
  name     = "rke-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = join(",", data.aws_vpcs.vpc_info.ids)
  
  health_check {
    enabled             = true
    path                = "/healthz"
    port                = 10254
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
  }

  tags = {
    Name = "rke-tg"
  }
}

# Get the VPC ID
data "aws_vpcs" "vpc_info" {
}

# Associate the EC2 to the target group
resource "aws_lb_target_group_attachment" "aws_tg_attachment" {
  count            = var.node_count

  target_group_arn = aws_lb_target_group.rke_tg.arn
  target_id        = aws_instance.rke-node[count.index].id
  port             = 80
}

# Create the http/80 listener
resource "aws_lb_listener" "elb_listener_80" {
  load_balancer_arn = aws_lb.rke_elb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# Create the https/443 listener
resource "aws_lb_listener" "elb_listener_443" {
  load_balancer_arn = aws_lb.rke_elb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.rke_tg.arn
  }
}

# Create a security group
resource "aws_security_group" "elb_sg" {
  name        = "elb-sg"
  description = "ELB SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name = "elb-sg"
  }
}