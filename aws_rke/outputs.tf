# Output the PEM file
output "private_key" {
  value = tls_private_key.node-key.private_key_pem
}

# Output the instance user
output "ssh_username" {
  value = "ubuntu"
}

# Output the instance public DNS names
output "addresses" {
  value = aws_instance.rke-node[*].public_dns
}

# Othe instance private IPs
output "internal_ips" {
  value = aws_instance.rke-node[*].private_ip
}