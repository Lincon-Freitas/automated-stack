# Creates the tls certificate for the domain
resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain
  subject_alternative_names = [format("*.%s",var.domain)]
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "cert_validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_record.fqdn]
}